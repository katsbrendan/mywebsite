from django.test import TestCase
from .models import Categories, Product, Type
from django.test import TestCase, Client
from django.urls import reverse
from .models import Categories, Product, Type
from django.urls import reverse, resolve
from .views import items, cart, checkout, detail

# Create your tests here.
class CategoriesModelTest(TestCase):
    def setUp(self):
        self.category = Categories.objects.create(category_name='Test Category')

    def test_category_name(self):
        self.assertEqual(str(self.category), 'Test Category')

class ProductModelTest(TestCase):
    def setUp(self):
        self.category = Categories.objects.create(category_name='Test Category')
        self.product = Product.objects.create(categories=self.category, name='Test Product')

    def test_product_name(self):
        self.assertEqual(str(self.product), 'Test Product')

class TypeModelTest(TestCase):
    def setUp(self):
        self.category = Categories.objects.create(category_name='Test Category')
        self.product = Product.objects.create(categories=self.category, name='Test Product')
        self.type = Type.objects.create(Category=self.product, name='Test Type', price=10.00)

    def test_type_name(self):
        self.assertEqual(str(self.type), 'Test Type')


class TestViews(TestCase):
    def setUp(self):
        self.client = Client()

    def test_items_view(self):
        response = self.client.get(reverse('jumia:items'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'jumia/item.html')

    def test_cart_view(self):
        response = self.client.get(reverse('jumia:cart'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'jumia/cart.html')

    def test_checkout_view(self):
        response = self.client.get(reverse('jumia:checkout'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'jumia/checkout.html')

    def test_detail_view(self):
        category = Categories.objects.create(category_name='Test Category')
        product = Product.objects.create(categories=category, name='Test Product')
        type = Type.objects.create(Category=product, name='Test Type', price=10.00)
        response = self.client.get(reverse('jumia:detail', args=[type.id]))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'jumia/detail.html')


class TestUrls(SimpleTestCase):
    def test_items_url_resolves(self):
        url = reverse('jumia:items')
        self.assertEquals(resolve(url).func, items)

    def test_cart_url_resolves(self):
        url = reverse('jumia:cart')
        self.assertEquals(resolve(url).func, cart)

    def test_checkout_url_resolves(self):
        url = reverse('jumia:checkout')
        self.assertEquals(resolve(url).func, checkout)

    def test_detail_url_resolves(self):
        url = reverse('jumia:detail', args=[1])
        self.assertEquals(resolve(url).func, detail)