from django.test import TestCase
from django.test import TestCase
from .models import Categories, Product, Type
from django.test import TestCase, Client
from django.urls import reverse
from .models import Categories, Product, Type
from django.test import SimpleTestCase
from django.urls import reverse, resolve
from .views import items, detail, cart, checkout

# Create your tests here.

class ModelTestCase(TestCase):
    def setUp(self):
        # Create test data
        self.category = Categories.objects.create(category_name='Test Category')
        self.product = Product.objects.create(categories=self.category, name='Test Product')
        self.type = Type.objects.create(
            business=None,
            Category=self.product,
            name='Test Type',
            description='Test Description',
            price=10.0,
            is_available=True,
            product_by_id=1
        )

    def test_type_str(self):
        self.assertEqual(str(self.type), 'Test Type')

class ViewTestCase(TestCase):
    def setUp(self):
        # Create test data
        self.category = Categories.objects.create(category_name='Test Category')
        self.product = Product.objects.create(categories=self.category, name='Test Product')
        self.type = Type.objects.create(
            business=None,
            Category=self.product,
            name='Test Type',
            description='Test Description',
            price=10.0,
            is_available=True,
            product_by_id=1
        )

    def test_items_view(self):
        client = Client()
        response = client.get(reverse('items'))
        self.assertEqual(response.status_code, 200)

class UrlsTestCase(SimpleTestCase):
    def test_items_url_resolves(self):
        url = reverse('items')
        self.assertEqual(resolve(url).func, items)