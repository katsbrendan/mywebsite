from django.contrib import admin

# Register your models here.
# admin.py
from .models import Categories, Product, Type

admin.site.register(Categories)
admin.site.register(Product)
admin.site.register(Type)
